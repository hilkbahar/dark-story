#!/usr/bin/env python
# coding=utf-8
import concurrent.futures
import os.path
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.options
from tornado.httpclient import AsyncHTTPClient
from handlers.action import ActionHandler
from tornado.options import define, options
import locale
import sys

locale._setlocale(locale.LC_ALL)

reload(sys)
sys.setdefaultencoding("utf-8")

define("config", default="develop", help="Config file type (prod, develop)")
define("port", default="8888", help="run on the given port", type=int)
define("app", default="com.darkstory", help="Application")
define("api_url", default='http://test.tv2.com.tr/api/', help="API URL")
define("host", default='test.tv2.com.tr', help="Host")
define("system_email", default='no-reply@darkstory.com', help="System Email")
define("contact_receiver_email", default='contact@darkstory.com', help="Contact Email")
define("app_id", default='592be746f5ac763b283cccbc', help="Application ID")
define("app_secret", default='ZaEU3HTS9dqvsa/ameEtAxQMmQtn9Mdtj0N/pV85Bn1y4x/3FV3q0/Ghc9fbDVzYirReycYgl2vZTSkymP0CGg==', help="Application Secret")
define("menu_id", default='592c14b0f5ac760dc026c35a', help="Menu ID")
define("cookie_secret", default='7facccf7-d747-4706-b623-192b0c3e91a7', help="Cookie Secret")
define("facebook", default='http://www.facebook.com', help="Facebook Social Account Link")
define("twitter", default='http://www.twitter.com', help="Twitter Social Account Link")
define("instagram", default='http://www.instagram.com', help="Instagram Social Account Link")
define("youtube", default='http://www.youtube.com', help="Youtube Social Account Link")
define("image_service", default='http://media.netd.com.tr', help="Image Service Link")
define("smtp_host", default='192.1.4.83', help="SMTP Host")
define("smtp_port", default=587, help="SMTP Port")
define("smtp_ssl", default=False, help="SMTP SSL")
define("smtp_username", default='ottnotification', help="SMTP Username")
define("smtp_password", default='p@ssw0rd', help="SMTP Password")
define("debug", default=True, help="Debug Mod")
define("draft_content", default=False, help="Draft Content Show")
define("default_connection_string", default="mongodb://192.1.4.173/quark_tv2", help="Mongo Connection String")

# A thread pool to be used for password hashing with bcrypt.
executor = concurrent.futures.ThreadPoolExecutor(6)

class Application(tornado.web.Application):
    def __init__(self):
        routes = [
            (r'/(favicon.ico)', tornado.web.StaticFileHandler, {"path": ""}),
            (r'/(robots.txt)', tornado.web.StaticFileHandler, {"path": ""}),
            (r'/action/(?P<action>[^\/]+)/?(?P<id>[^\/]+)?', ActionHandler)
        ]
        settings = dict(
            title=u"Dark Story",
            compress_response=True,
            xsrf_cookies=False,
            cookie_secret=options.cookie_secret,
            facebook=options.facebook,
            twitter=options.twitter,
            instagram=options.instagram,
            youtube=options.youtube,
            login_url="/auth/login",
            debug=options.debug,
            autoreload=True,
            system_email=options.system_email,
            contact_receiver_email=options.contact_receiver_email,
            quark=dict(
                api_url=options.api_url,
                host=options.host,
                app_id=options.app_id,
                app_secret=options.app_secret,
                menu_id=options.menu_id,
                app_name=options.app,
                draft_content=options.draft_content,
                image_service=options.image_service
            ),
            smtp=dict(
                host=options.smtp_host,
                port=options.smtp_port,
                ssl=options.smtp_ssl,
                username=options.smtp_username,
                password=options.smtp_password
            )
        )
        #AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient", max_clients=100);
        super(Application, self).__init__(routes, **settings)

def main():
    options.parse_command_line()
    options.parse_config_file('%s.conf' % options.config)
    options.parse_command_line()

    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
