import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'
import tr from 'vee-validate/dist/locale/tr'

// Add locale helper.
Validator.addLocale(tr)

const config = {
  delay: 0,
  locale: 'tr'
}

Vue.use(VeeValidate, config)
