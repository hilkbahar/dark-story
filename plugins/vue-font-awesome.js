import Vue from 'vue'

// or import all icons if you don't care about bundle size
import 'vue-awesome/icons'

// globally (in your main .js file)
import Icon from 'vue-awesome/components/Icon.vue'

Vue.component('icon', Icon)
