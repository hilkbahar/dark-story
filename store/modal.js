import axios from 'axios'

export const state = {
  modalContent: null,
  modalRootContent: null,
  visible: false,
  id: '',
  hasFooter: true
}

export const getters = {
  getModalState: (state) => {
    return state.visible
  },
  getContent: (state) => {
    return state.modalContent
  },
  getId: (state) => {
    return state.id
  },
  hasFooter: (state) => {
    return state.hasFooter
  },
  getRootContent: (state) => {
    return state.modalRootContent
  }
}

export const mutations = {
  OPEN_MODAL (state) {
    state.visible = true
    document.querySelector('body').classList.add('modal-open')
  },
  CLOSE_MODAL (state) {
    state.visible = false
    state.modalContent = null
    state.id = ''
    document.querySelector('body').classList.remove('modal-open')
  },
  SET_MODAL_CONTENT (state, content) {
    state.modalContent = content
  },
  SET_MODAL_ROOT_CONTENT (state, rootContent) {
    state.modalRootContent = rootContent
  },
  SET_MODAL_ID (state, id) {
    state.id = id
  },
  SET_HAS_FOOTER (state) {
    state.hasFooter = (state.modalContent.CustomType !== 'Artist')
  }
}

export const actions = {
  async fetchUrl ({dispatch}, path) {
    let {data} = await axios.get(`${process.env.apiBaseUrl}/getcontent?url=${path}`)
    if (data.Content.length > 0) {
      dispatch('setContent', [data.Content[0], null])
    } else {
      throw new Error('Page Not Found')
    }
  },
  async fetchIxName ({dispatch}, ixname) {
    console.log(ixname)
    let {data} = await axios.get(`${process.env.apiBaseUrl}/getcontent?ixname=${ixname}`)
    if (data.Content.length > 0) {
      dispatch('setContent', [data.Content[0], null])
    } else {
      throw new Error('Page Not Found')
    }
  },
  setContent ({commit}, [content, rootContent]) {
    commit('SET_MODAL_ROOT_CONTENT', rootContent)
    commit('SET_MODAL_CONTENT', content)
    commit('SET_MODAL_ID', content.id)
    commit('SET_HAS_FOOTER')
    commit('OPEN_MODAL')
  },
  open ({state, commit}) {
    if (!state.visible) {
      commit('OPEN_MODAL')
    }
  },
  close ({commit}) {
    commit('CLOSE_MODAL')
  }
}
