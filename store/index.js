import axios from 'axios'

export const state = {
  sideNavToggle: false,
  header: [],
  footer: []
}

export const getters = {
  getHeader: (state) => {
    return state.header
  },
  getFooter: (state) => {
    return state.footer
  }
}

export const mutations = {
  TOGGLE_SIDENAV (state) {
    state.sideNavToggle = !state.sideNavToggle
  },
  SET_HEADER (state, header) {
    state.header = header
  },
  SET_FOOTER (state, footer) {
    state.footer = footer
  }
}

export const actions = {
  async fetchLayout ({commit}) {
    let {data} = await axios.get(`${process.env.apiBaseUrl}/getcontent?general=true`)
    commit('SET_HEADER', data.Header)
    commit('SET_FOOTER', data.Footer)
  }
}
