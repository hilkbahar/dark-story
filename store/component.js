import axios from 'axios'

export const state = {
  content: null
}

export const getters = {
  getTemplateName: (state) => {
    if (state.content) return (state.content.Template.IxName !== 'MainPage') ? state.content.Template.IxName : null
  },
  getContent: (state) => {
    return state.content
  },
  getRegions: (state) => {
    if (state.content) return state.content.Template.Regions
  }
}

export const mutations = {
  SET_CONTENT: function (state, content) {
    state.content = content
  }
}

export const actions = {
  async fetch ({state, commit}, path) {
    let {data} = await axios.get(`${process.env.apiBaseUrl}/getcontent?url=${path}`)
    if (data.Content.length > 0) {
      commit('SET_CONTENT', data.Content[0])
    } else {
      throw new Error('Page Not Found')
    }
  }
}
