export const state = {
  introductionSlides: {
    576: {
      slidesPerView: 1.5,
      spaceBetween: 20
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 20
    }
  },
  photoGallerySlides: {
    576: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    768: {
      slidesPerView: 1.5,
      spaceBetween: 20
    }
  },
  artistsSlides: {
    576: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 20
    }
  }
}

export const getters = {
  introductionSlides: state => state.introductionSlides,
  photoGallerySlides: state => state.photoGallerySlides,
  artistsSlides: state => state.artistsSlides
}

