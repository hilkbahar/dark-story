export default function ({store}) {
  if (store.state.sideNavToggle) {
    store.commit('TOGGLE_SIDENAV')
    document.getElementsByTagName('body')[0].className = ''
  }
}
