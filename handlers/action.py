#!/usr/bin/env python
# -*- coding: utf-8 -*-

#region References
import tornado
import os
import json
import handlers
from email.header import Header
from email.utils import formataddr
from handlers import staticfiles
from slugify import slugify
from tornado import web
from helpers.decorations import allow_method
from helpers.quark import QuarkHelper
from helpers import uihelpers
from tornado import gen
from bson import json_util
#endregion

class ActionHandler(handlers.BaseHandler):

    quark_helper = None
    content_helper = None
    params=None

    def get_body_as_json(self):
        try:
            return json.loads(self.request.body)
        except ValueError as e:
            return None

    @gen.coroutine
    def prepare(self):
        super(ActionHandler, self).prepare()
        self.quark_helper = QuarkHelper(self)

    @gen.coroutine
    def options(self, action=None, id=None):
        self.set_default_headers()

    @gen.coroutine
    def get(self):
        pass

    @gen.coroutine
    def get(self, **params):
        self.params=params
        methodToCall = getattr(self, params['action'])
        yield methodToCall()
        self.set_default_headers()

    @gen.coroutine
    def post(self, **params):
        self.params=params
        methodToCall = getattr(self, params['action'])
        yield methodToCall()

    @gen.coroutine
    @allow_method("POST")
    def send_contact_form(self):
        self.set_header("Content-Type", "application/json")

        try:
            data = json.loads(self.request.body)

            if data:
                data['Sender'] = formataddr((str(Header('Dark Story Contact Form', 'utf-8')), self.quark_helper.handler.application.settings['system_email']))
                data['Subject'] = data['Subject'] if data.get('Subject') else "Web İletişim Formu"
                data['Receivers'] = self.quark_helper.handler.application.settings['contact_receiver_email']

                send_mail = yield uihelpers.send_mail(self,data)
                message = "Success"
            else:
                send_mail = False
                message = "Data Required"

        except Exception, ex:
            send_mail = False
            message = ex.message

        self.write(json.dumps({"SendMail": send_mail, "Message": message}, default=json_util.default))

    @gen.coroutine
    def getcontent(self):

        self.set_header("Content-Type", "application/json")

        try:

            response = { "Header": { "Menu": [], "SocialLinks": []  }, "Content": [], "Footer" : { "Left": {}, "Right": {} } }

            content_id = self.get_argument('id', None)
            url = self.get_argument('url', None)
            ixname = self.get_argument('ixname', None)
            general = self.get_argument('general', None)

            if general == "true":

                menuLayout = yield self.quark_helper._get('supercontents/' + self.quark_helper.handler.application.settings['quark']['menu_id'])
                headerMenuItems = uihelpers.get_active_views(uihelpers.get_controls(menuLayout, "HeaderMenu")[0])
                footerLeft = uihelpers.get_active_views(uihelpers.get_controls(menuLayout, "FooterLeft")[0])
                footerRight = uihelpers.get_active_views(uihelpers.get_controls(menuLayout, "FooterRight")[0])

                if headerMenuItems:
                    for item in headerMenuItems:
                        item = item["Content"]
                        response["Header"]["Menu"].append({"Name": item["Title"], "Url": item["Url"]})

                if footerLeft and footerRight:
                    if footerLeft:
                        response["Footer"]["Left"] = footerLeft[0]["Title"]
                    if footerRight:
                        response["Footer"]["Right"] = footerRight[0]["Title"]

                socialTypeList = ["facebook", "twitter", "instagram", "youtube"]

                for socialType in socialTypeList:
                    response["Header"]["SocialLinks"].append({"Name": socialType, "Url": self.quark_helper.handler.application.settings[socialType]})
            else:
                del response["Header"]
                del response["Footer"]

            if content_id or url or ixname:

                query = None

                if content_id:
                    query = uihelpers.query_creater(service_prefix = "supercontents", filter = "Id eq '" + content_id + "' and Status eq 'Active'", top=1)
                elif url:
                    contentData = yield self.quark_helper._get_by_url(url)
                elif ixname:
                    query = uihelpers.query_creater(service_prefix="supercontents", filter="IxName eq '" + ixname + "' and Status eq 'Active'", top=1)
                else:
                    contentData = None

                if query:
                    contentData = yield self.quark_helper._get(query)

                    if contentData:
                        if type(contentData) is list:
                            contentData = contentData[0]

                if contentData:

                    self.prepare_controls(contentData)
                    yield self.tasks



                    response["Content"].append({
                        "id": contentData["_Id"],
                        "ContentType": contentData["ContentType"],
                        "CustomType": contentData["CustomType"] if contentData.has_key("CustomType") else None,
                        "Title": contentData["Title"],
                        "Text": contentData["Text"],
                        "Description": contentData["Description"],
                        "Url": contentData["Url"],
                        "ContentTags": contentData["ContentTags"],
                        "Files": contentData["Files"],
                        "MediaFiles": contentData["MediaFiles"] if contentData.has_key("MediaFiles") else None,
                        "IxName": contentData["IxName"],
                        "Template": contentData["Template"] if contentData.has_key("Template") else None,
                        "Status": contentData["Status"],
                        "CreatedBy": contentData["CreatedBy"],
                        "CreatedDate": contentData["CreatedDate"],
                        "ModifiedBy": contentData["ModifiedBy"],
                        "ModifiedDate": contentData["ModifiedDate"]
                    })

        except Exception, ex:
            self.set_status(500)
            response = {"Error": { "Code": "500", "Description": ex.message }}

        self.write(json.dumps(response, default=json_util.default))

    @gen.coroutine
    def media(self):

        id = self.get_argument('id', None)

        self.set_header("Content-Type", "application/json")

        if id:
            user_ip = self.request.headers.get("X-Forwarded-For", self.request.remote_ip)
            user_ip = self.request.headers.get("X-Cip", user_ip)
            user_ip = self.request.headers.get("X-Real-Ip", user_ip)
            user_ip = user_ip.split(',')[-1].strip()

            model = yield self.quark_helper._get("supercontents/" + id)

            if model.get('MediaFiles'):

                path = model['MediaFiles'][0]['Path']
                api_url = self.application.settings['quark']['api_url']

                filename, ext = os.path.splitext(path)

                controller = "oms"
                buffertime = 32
                app = "com.darkstory"
                min = 0
                max = 1250

                subtitles = []
                if model.get('SubTitles'):
                    for subtitle in [x for x in model['SubTitles'] if x.get('File') and x['File'].get('_Id')]:
                        subtitles.append(
                            {
                                'src': "http://ctq.dogannet.tv/" + "file/get/" + subtitle['File']['_Id'],
                                'label': subtitle['Language']
                            }
                        )

                url = "v2/medialink?ip=" + user_ip + "&path=" + path;
                url = url + "&live=true" if model['ContentType'] == 'Live' else url

                media_link = yield self.quark_helper._get(url)

                if str(path).startswith("http"):
                    media_link['ServiceUrl'] = ""
                    media_link['SecurePath'] = path

                result = {
                    "Id": model['_Id'],
                    "Url": model.get('Url'),
                    "Media": {
                        "Controller": controller,
                        "BuderTime": buffertime,
                        "Link": media_link,
                        "Image": self.application.settings['quark']['image_service'] + "/100/800x450/" + model['Files'][0]['_Id'] if model.get('Files') else ''
                    },
                    "User": {"Name": "", "SessionId": "", "AnonymId": "", "IsAuthenticated": "", "HasPackage": ""},
                    "Package": None,
                    "Ad": "false",
                    "Subtitles": subtitles
                }

                self.write(json.dumps(result, default=json_util.default))
            else:
                self.write(json.dumps('{"Error":"MediaFiles hatası"}', default=json_util.default))
