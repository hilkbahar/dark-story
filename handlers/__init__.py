# coding=utf-8

import tornado
from tornado import gen
import json
from helpers.quark import QuarkHelper
from services.control import ControlService

class BaseHandler(tornado.web.RequestHandler):

    control_service = None 
    quark_helper = None
    content_helper = None

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT')

    def options(self):
        self.set_status(204)
        self.finish()

    @property
    def db(self):
        return self.application.db

    @property
    def fs(self):
        return self.application.fs
            
    def prepare(self):
        self.quark_helper = QuarkHelper(self)
        self.control_service = ControlService(self)
        self.tasks = []

    @gen.coroutine
    def __prepare_control(self, control, content):
        method_to_call = getattr(self.control_service, control['IxName'])
        task = method_to_call(control, content)
        self.tasks.append(task)

        if control.get('Controls'):
            for sub_control in control['Controls']:
                self.__prepare_control(sub_control, content)

    @gen.coroutine
    def prepare_controls(self, content):
        if not content:
            return
        if content.get('Template'):
            if content['Template'].get('Regions'):
                for region in content['Template']['Regions']:
                    if region.get('Controls'):
                        for control in region['Controls']:
                            self.__prepare_control(control, content)

    def _qerror_to_error_response(self, ex):

        self.set_status(ex.status_code)
        self.set_header("Content-Type", 'application/json; charset="utf-8"')

        if ex.message:
            self.write(json.dumps({
                "error": {
                    "message": ex.message,
                    "errors": ex.errors
                }
            }))

        self.finish()