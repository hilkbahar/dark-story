import json


def get_file(file_name):
    file = open("static_files/" + file_name)
    result = json.load(file)
    file.close()

    return result

#cities= get_file("cities.json")