#!/usr/bin/env python
# coding=utf8

import concurrent.futures

executor = concurrent.futures.ThreadPoolExecutor(2)

class BaseService(object):

    application = None
    db = None
    fs = None

    def __init__(self, application):
        self.application = application
        self.db = application.db
        self.fs = application.fs