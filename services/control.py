# coding=utf-8

#region References

from tornado import gen
from helpers import uihelpers
from helpers.quark import QuarkHelper

import sys
import random

#endregion

reload(sys)
sys.setdefaultencoding('utf-8')

class ControlService:

    handler = None
    quark_helper = None

    def __init__(self, handler):
        self.handler = handler
        self.quark_helper = QuarkHelper(self.handler)

    def __get_property_value(property):
        pass

    @gen.coroutine
    def VideoPlayerV4(self,control,content):
        if content.get('ContentType') == 'TVShowContainer':
            query = uihelpers.query_creater(content_types="TVShow", paths= content['PathString'] + content['IxName']+"/", top=1, order_by="StartDate desc")
            control['LastContents'] = yield self.quark_helper._get(query)

        if uihelpers.get_property(content, 'VideoEndContent') != 'false' and content.get('ContentTags'):
            tags = ','.join([x.get('IxName') for x in content.get('ContentTags')])
            top=10
            content_types= "NewsVideo,TVShow"
            _filter = "StartDate ge datetime'" + content.get('StartDate') + "'"
            _filter_for_old = "StartDate le datetime'" + content.get('StartDate') + "'"
            tasks = []
            query = uihelpers.query_creater(filter=_filter, content_types=content_types, order_by="StartDate asc",
                                            tags=tags, top=top)
            query_for_old = uihelpers.query_creater(filter=_filter_for_old, content_types=content_types,
                                                    order_by="StartDate desc", tags=tags, top=top)
            items = self.quark_helper._get(query)
            tasks.append(items)

            items_old = self.quark_helper._get(query_for_old)
            tasks.append(items_old)

            yield tasks

            items = random.shuffle(items_old.result()) if not items.result() else items.result()+ items_old.result()

            control['VideoEndContents'] = [x for x in items if x['_Id'] != content['_Id']]

        pass

    @gen.coroutine
    def EditorControl(self, control, content):

        try:

            viewType = uihelpers.get_property(control, 'ViewType')
            controlData = control.get('ContentViews')

            if viewType == "MainSlider" and controlData:
                for item in control.get('ContentViews'):

                    page = yield self.quark_helper._get_by_url(item.get('Content').get('Url'))
                    pageList = []

                    if page.get("Relations"):
                        pageList = page.get("Relations")
                    else:
                        pageList.append(page)

                    if pageList:

                        if item["Content"]["Relations"]:
                            item["Content"]["Relations"] = []

                        for current in pageList:
                            item["Content"]["Relations"].append({
                                "_Id": current["_Id"],
                                "ContentType": current["ContentType"],
                                "CustomType": current["CustomType"],
                                "IxName": current["IxName"],
                                "Title": current["Title"],
                                "Url": current["Url"],
                                "MediaFiles": current["MediaFiles"],
                                "HeadlineText": uihelpers.get_property(current, "HeadlineText") if uihelpers.get_property(current, "HeadlineText") else current["Title"]
                            })

            if controlData:
                for data in controlData:
                    if data["Content"]:

                        DescriptionShow = uihelpers.get_property(data["Content"], 'DescriptionShow')

                        if DescriptionShow:
                            if DescriptionShow == "true":
                                data["Content"]["DescriptionShow"] = True
                            else:
                                data["Content"]["DescriptionShow"] = False
                        else:
                            data["Content"]["DescriptionShow"] = True

        except Exception, ex:
            print ex.message

    @gen.coroutine
    def ContentText(self, control, content):
        pass

    @gen.coroutine
    def HeaderMenu(self, control, content):
        pass

    @gen.coroutine
    def Contact(self, control, content):
        pass