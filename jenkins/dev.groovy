base_path = '/var/www'
domain = 'darkstory.com'
path = "${base_path}/${domain}"

builds = []
def lastSuccessfullBuild(build) {
    if(build != null) {
        //Add the build to the array
        builds.add(build);
        lastSuccessfullBuild(build.getPreviousBuild());
    }
}
lastSuccessfullBuild(currentBuild.getPreviousBuild());

node('192.1.5.187') {
    stage('git') {
        git branch: 'develop',
            credentialsId: 'eef13b06-9ebc-4845-8fbd-4db3e846ebee',
            url: 'https://dogantv_jenkins@bitbucket.org/dtvh/dark_story.git'
    }
   
   stage('clear node_modules'){
        sh 'rm -rf node_modules'
    }
    stage('npm cache clean'){
        sh 'npm cache clean'
    }
    // pull dependencies from npm
    stage('npm install'){
        sh 'npm install'
    }
    stage('npm run build'){
        sh 'npm run build'
    }
    stage('npm start'){
        sh 'npm start &'
    }
/*
    stage('npm restart'){
        sh 'npm restart'
    }*/
    
    
    stage('virtualenv') {
        if (!fileExists('venv/')) {
            sh "virtualenv venv"
        }
        sh """
        source venv/bin/activate
        pip install  -r requirements.txt
        """
    }
    stage('stash'){
        stash name: 'deploy_package',
              excludes: 'node_modules/**',
              includes: '**'
    }
}

stage('deploy_to_dev'){
    parallel darkstory_dev: {
        deploy('192.1.5.187')
    },
    failFast: true
}

stage('clean_old_builds'){
    parallel darkstory_dev: {
        clean('192.1.5.187')
    }, 
    failFast: true
}

node('master'){
    notify('Deploy succeeded')
}

def deploy(agent) {
    node(agent) {
        dir("${path}/dev_v${env.BUILD_NUMBER}") {
            unstash 'deploy_package'
        }
        sh "sudo ln -sfn ${path}/dev_v${env.BUILD_NUMBER} ${base_path}/dev.${domain}"
        sh 'sudo supervisorctl restart darkstory_dev:*'
    }
}

def clean(agent){
    node(agent) {
        for(i = 3; i<builds.size();i++){
            build = builds[i];
            if(fileExists("${path}/dev_v${build.number}/")){
                sh "rm -rf ${path}/dev_v${build.number}"
            }
        }
    }
}

def notify(status){
    emailext (
      to: "can.ilkbahar@dogantv.com.tr,earslan@dogantv.com.tr,onur.keskin@dogantv.com.tr,murat.dinc@dogantv.com.tr",
      subject: "${status}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: """
        ${status}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':
        Check console output at ${env.BUILD_URL}
      """,
    )
}    
/*      
    // run grunt
    stage('grunt'){
        sh 'grunt all'
    }
 
    // create virtualenv
    stage('virtualenv'){
        if(!fileExists('venv/')){
            sh "virtualenv venv"
        }
        sh """
            source venv/bin/activate
            pip install -r requirements.txt
        """
    }
*/
    // stash code & dependencies to expedite subsequent testing
    // and ensure same code & dependencies are used throughout the pipeline
    // stash is a temporary archive