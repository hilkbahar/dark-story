module.exports = {
  env: {
    apiBaseUrl: "http://localhost:9000/action",
    mediaBaseUrl: 'http://test.tv2.com.tr',
    siteUrl: 'http://darkstory.com.tr'
  },
  router: {
    extendRoutes (routes, resolve) {
      routes.push(
        {
          name: 'custom',
          path: '/(.*?)',
          component: resolve(__dirname, 'partial/custom.vue')
        }
      )
    }
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'Dark Story',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Nuxt.js project'},
      {property: 'og:url', content: 'http://www.darkstory.com.tr'},
      {property: 'og:type', content: 'website'},
      {property: 'og:title', content: 'Dark Story'},
      {property: 'og:description', content: 'Dark story desctipyion'},
      {property: 'og:image', content: '/dark_story_fb_placeholder.jpg'}
    ],
    htmlAttrs: {
      lang: 'tr'
    },
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  /*
   ** Global CSS
   */
  css: [{src: '~assets/css/style.scss', lang: 'scss'}],
  /*
   ** Customize the progress-bar color
   */
  loading: {color: '#FFF'},
  /*
   ** Build configuration
   */
  build: {
    vendor: ['vue-awesome', 'axios'],
    /*
     ** Run ESLINT on save
     */
    extend (config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    {src: '~plugins/vue-swiper.js', ssr: true},
    {src: '~plugins/vue-validation.js', ssr: true}
  ]
}
