import simplejson as simplejson
import tornado
import tornado.httpclient
from tornado.httpclient import AsyncHTTPClient
from tornado import gen
import urlparse
from hashlib import sha1
import hmac
import concurrent.futures
import json, pprint

class QuarkHelper:

    handler = None
    # A thread pool to be used for password hashing with bcrypt.
    executor = concurrent.futures.ThreadPoolExecutor(2)

    def __init__(self, handler):
        self.handler = handler

    @gen.coroutine
    def __get_date(self):
        request = tornado.httpclient.HTTPRequest(
            method='GET',
            url=urlparse.urljoin(
                self.handler.application.settings['quark']['api_url'],
                'date'
            ),
            headers={
                'Accept': 'application/json'
            }
        )
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(request)

        raise gen.Return(response.body.replace('"', ''))

    @gen.coroutine
    def __sign(self, date):
        key = self.handler.application.settings['quark']['app_secret']

        hashed = yield self.executor.submit(hmac.new, key, date, sha1)

        # The signature
        raise gen.Return(hashed.digest().encode("base64").rstrip('\n'))

    @gen.coroutine
    def _get(self, url):
        date = yield self.__get_date()
        signed = yield self.__sign(date)

        request = tornado.httpclient.HTTPRequest(
            method='GET',
            url=urlparse.urljoin(
                self.handler.application.settings['quark']['api_url'],
                url
            ),
            headers={
                'Authorization':
                self.handler.application.settings[
                    'quark']['app_id'] + ':' + signed,
                'Date': date,
                'Accept': 'application/json'
            },
            connect_timeout=30
        )

        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(request)

        raise gen.Return(json.loads(response.body))

    @gen.coroutine
    def _get_by_url(self, url):
        data = yield self._get('contents/getbyurl()?url=%s' % url)

        if data:
            raise gen.Return(data)

    @gen.coroutine
    def _post(self, url, body):

        date = yield self.__get_date()
        signed = yield self.__sign(date)

        request = tornado.httpclient.HTTPRequest(
            method='POST',
            url=urlparse.urljoin(
                self.handler.application.settings['quark']['api_url'],
                url
            ),
            headers={
                'Authorization':
                self.handler.application.settings[
                    'quark']['app_id'] + ':' + signed,
                'Date': date,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body=json.dumps(body)
        )

        _json = None
        response = None
        try:
            http_client = AsyncHTTPClient()
            response = yield http_client.fetch(request)
        except:
            _json = json.loads('{"error": {"message": "API.ERROR"}}')

        if response:
            if len(response.body) > 3:
                _json = json.loads(response.body)
            else:
                if response.code >= 200 and response.code < 300:
                    _json = json.loads('{"status": true}')
                else:
                    _json = json.loads('{"error": {"message": "API.ERROR"}}')

        if _json:
            raise gen.Return(_json)

    @gen.coroutine
    def _put(self, url, data):
        date = yield self.__get_date()
        signed = yield self.__sign(date)
        url = urlparse.urljoin(
            self.handler.application.settings['quark']['api_url'],
            url
        )
        request = tornado.httpclient.HTTPRequest(
            method='PUT',
            url=url,
            headers={
                'Authorization':
                self.handler.application.settings[
                    'quark']['app_id'] + ':' + signed,
                'Date': date,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body=simplejson.dumps(data)
        )
        _json = None

        try:
            http_client = AsyncHTTPClient()
            response = yield http_client.fetch(request)
            _json = json.loads(response.body)
        except:
            _json = json.loads('{"error": {"message": "API.ERROR"}}')

        if _json:
            raise gen.Return(_json)

    @gen.coroutine
    def _delete(self, url):
        date = yield self.__get_date()
        signed = yield self.__sign(date)

        request = tornado.httpclient.HTTPRequest(
            method='DELETE',
            url=urlparse.urljoin(
                self.handler.application.settings['quark']['api_url'],
                url
            ),
            headers={
                'Authorization':
                self.handler.application.settings[
                    'quark']['app_id'] + ':' + signed,
                'Date': date,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        )
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(request)

        try:
            raise gen.Return(json.loads(response.body))
        except:
            pass
