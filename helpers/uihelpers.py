# coding=utf-8
# encoding=utf8
import smtplib
from datetime import datetime, timedelta
import json
import re
import urllib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sys

from bs4 import BeautifulSoup
from tornado import gen
from dateutil.parser import parse
import pytz
from slugify import slugify
import locale

from handlers import staticfiles

tz = pytz.timezone("Turkey")

def medyanet_keywords(data):
    if data.get('ContentTags'):
        keywords = ','.join([x['IxName'] for x in data['ContentTags']])
        keywords = re.sub("^\s+", "", keywords)
        keywords = re.sub("\s+$", "", keywords)
        keywords = re.sub("\s+", "_", keywords)
        keywords = re.sub("(?![a-z 0-9 _ ,]).", "", keywords)

        return keywords
    return ""

def get_controls(data, region_name):
    controls = [region.get('Controls') for region in data['Template'][
        'Regions'] if region['IxName'] == region_name]
    if controls and len(controls) > 0:
        return controls[0]

def remove_html_tags(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

def title_except(s, exceptions):
    word_list = re.split(' ', s)       # re.split behaves as expected
    final = [word_list[0].capitalize()]
    for word in word_list[1:]:
        final.append(word if word in exceptions else word.capitalize())
    return " ".join(final)

def format_date(date, format='%d.%m.%Y %A %H:%M' , l='tr_TR', time_zone="Asia/Baghdad"):
    #utc_date = parse(date)
    #local_date = utc_date.replace(tzinfo=pytz.utc).astimezone(tz)
    #return local_date.strftime(format)
    utc_date = parse(date)
    if time_zone:
        tz = pytz.timezone(time_zone)
        local_date = utc_date.replace(tzinfo=pytz.utc).astimezone(tz)
    else:
        local_date = utc_date.replace(tzinfo=pytz.utc)
    locale.setlocale(locale.LC_ALL, l)
    return local_date.strftime(format).decode(locale.getlocale()[1])

def control_active_view(data,index):
    result = False
    try:
        view_data = data[int(index)]
        result = True
    except:
        result = False
    return result

def diff_date(start_date, end_date=None, format="diff"):
    now = datetime.utcnow() if end_date == None else parse(end_date).replace(tzinfo=None)
    delta = now - parse(start_date).replace(tzinfo=None)
    sec = delta.total_seconds()
    min = int(sec / 60)
    hour = int(min/60)
    day = int(hour/24)
    if format == "diff":
        _time = str(sec) + " saniye önce" if sec > 0 else ""
        _time = str(min) + " dakika önce" if min > 0 else _time
        _time = str(hour) + " saat önce" if hour > 0 else _time
        _time = str(day) + " dakika önce" if day > 0 else _time
        return _time

    elif format == "min":
        return int(sec/60)

    elif format == "sec":
        return int(sec)

def format_writers(writers):
    return ' / '.join([w.get('Fullname') for w in writers])

def get_active_views(data):
    if data.get('ContentViews') and len(data['ContentViews']) > 0:
        if(data['ContentViews'][0].get('Content')):
            data['ContentViews'] = filter(lambda x: x['Content']['Status'] == 'Active', data['ContentViews'])
        elif (data['ContentViews'][0].get('Content')):
            data['ContentViews'] = filter(lambda x: x['Status'] == 'Active', data['ContentViews'])
        return data['ContentViews']
    else:
        return []

def get_ancestors(data):
    ancestors=[]
    if data:
        if data.get('Content') and data.get('Content').get('Ancestors'):
            ancestors = filter(lambda x: x['IxName'] != 'root', data['Content']['Ancestors'])

        elif data.get('Ancestors') and len(data['Ancestors']) > 0:
            ancestors = filter(lambda x: x['IxName'] != 'root', data['Ancestors'])

        return ancestors
    return []

def get_attr(data, attr):
    result=''
    if data.get('Content') and data.get('Content').get(attr):
        result = data['Content'][attr]

    elif data.get(attr) and len(data[attr]) > 0:
        result = data[attr]

    return result

def get_property(data, propName):
    if data and data.get('Properties') and len(data['Properties']) > 0:
        list = filter(lambda x: x['IxName'] == propName, data['Properties'])
        if list is not None and len(list)>0:
            p = list[0]

            if p['Type']==1 or p['Type'] == 0 or p['Type'] == 2:
                return p['Value']
            elif p['Type'] == 4:
                p = filter(lambda x: x['Selected'] == True, p['SelectValues'])
                if p:
                    return p[0]['IxName']
            elif p['Type'] == 5:
                p = filter(lambda x: x['Selected'] == True, p['SelectValues'])
                if p:
                    return p
    return ""

def get_image_url(self, data, position=0, w=0, h=0):
    if data.get('Files') and len(data['Files']) > position:
        return "{0}/{1}x{2}/{3}".format(self.application.settings['quark']['image_service'], w, h, data['Files'][0]['_Id'])
    else:
        return ""

def query_creater(service_prefix="supercontents/active()", filter="", content_types="",
                  custom_types="", paths="", order_by="", top=None, skip=None, tags="", sub_path='true', start_date=None):
    query= service_prefix+"?"

    _qList=""

    if start_date:
        date_filter = "StartDate ge datetime'" + start_date +"'"
        filter += " and " + date_filter if filter else date_filter

    if sub_path=='true' and  paths:
        path= paths.split(',')[0]
        path_filter = "Ancestors/any(a:a/SelfPath eq '" + path + "')"
        filter += " and " + path_filter if filter else path_filter
        paths = ""

    if(filter):
        query = query+"$filter="+filter



    if content_types and custom_types:
        content_types = "ContentType:{$in:['"+content_types.replace(",","','")+"']}"
        custom_types = "CustomType:{$in:['"+custom_types.replace(",","','")+"'']}"
        _qList = "$or:[{"+content_types+"},{"+custom_types+"}]"

    elif content_types:
        content_types="ContentType:{$in:['"+content_types.replace(",","','")+"']}"
        _qList = content_types
    elif custom_types:
        custom_types="CustomType:{$in:['"+custom_types.replace(",","','")+"'']}"
        _qList = custom_types

    if paths:
        if sub_path != 'true':
            paths = "Path:{$in:['" + paths.replace(",","','")+ "']}"
            _qList = _qList + "," + paths if _qList else paths


    if tags:
        tags = tags.replace("'", "")
        tags_list = ','.join([slugify(x.decode('utf-8')).lower() for x in tags.split(',')])

        tags = "'ContentTags.IxName':{$in:['" + tags_list.replace(",","','")+ "']}"
        _qList = _qList + "," + tags if _qList else tags

    if _qList and len(_qList)>0:
        query= query+"&q={"+_qList+"}"

    if order_by:
        query= query+"&$orderby="+order_by

    if top:
        query= query+"&$top="+str(top)

    if skip:
        query= query+"&$skip="+str(skip)

    query = urllib.quote(query.encode("utf-8"),"/.$?&=()[]:{}")

    return query

def get_url(content, control=None):
    if content.get('Content'):
        if content.get('Url'):
            return content.get('Url')
        elif content['Content'].get('Url'):
            return content['Content'].get('Url')

    return content.get('Url')

def get_configs_content(content, name='', type=""):

    #print "uihelpers get_configs_content"
    if type=="common":
        file = staticfiles.common_config
        return file
    if content:
        ancestors = get_ancestors(content)
        ancestor = ancestors[len(ancestors)-1]['SelfPath'] if ancestors else ''
        path=ancestor
        content_type = get_attr(content, 'ContentType')

        if(content_type=="Folder" or content_type=="TVShowContainer"):
            ixname = get_attr(content, 'IxName')
            path= path + ixname + "/" if path else "/"+ixname+"/"

        elif(content_type=="Page"):
            path = get_attr(content, 'Url')

        configs = staticfiles.configs_contents
        config = filter(lambda x: re.match(x['path'], path) and (content_type in x['content_type'] or x['content_type']==".*")  , configs)
        if config:
            if name:
                return config[0][name]
            else:
                return config[0]

    return None

def get_categories(ixname=None, order_field='Title'):
    categories = staticfiles.categories

    model = sorted(categories, key=lambda k: k[order_field], reverse=False)
    if ixname:
        model = [x for x in model if x['IxName'] == ixname]
        if model:
            model=model[0]
        else:
            model =None

    return model

def get_static_file(file=None, order_field='IxName'):
    file = open("static_files/" + file)
    model = json.load(file)
    return model

def get_service_url(self, service='image_service', type='ssl'):
    host = self.application.settings['quark'][service]
    if host and str(host).startswith('http'):
        return host
    elif host:
        return 'https:' + host if type=="ssl" else 'http:' + host

    return ""

@gen.coroutine
def send_mail(self, data):
    try:
        sender = data['Sender']
        receivers = data['Receivers']
        message = MIMEMultipart('alternative')
        _body = ''

        try:
            fields = data['Message'].iteritems()
            for key, value in fields:
                _body += '<br/><b>' + key + "</b>: " + value
            body = MIMEText(_body.encode('utf-8'), 'html')
        except:
            body = MIMEText(str(data['Message']).encode('utf-8'), 'html')

        message['Subject'] = data['Subject']
        message.attach(body)

        smtpObj = smtplib.SMTP(self.application.settings['smtp']['host'],self.application.settings['smtp']['port'])
        smtpObj.login(self.application.settings['smtp']['username'], self.application.settings['smtp']['password'])
        smtpObj.sendmail(sender, receivers, message.as_string())

        return True

    except:
        e = sys.exc_info()[0]
        return False

def get_html_tag_attr(source, tag, attr):
    soup = BeautifulSoup(source, 'html.parser')
    tags = soup.findAll(tag)
    _attr=""
    if(tags):
        _attr = str(tags[0][attr])
    return _attr