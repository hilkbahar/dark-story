
def allow_method(method_name):
    def allow_method_decorator(func):
        def func_wrapper(self, *args, **kwargs):
            if self.request.method == method_name:
                return func(self, *args, **kwargs)
            else:
                self.write("Not Allowed Method!")
        return func_wrapper
    return allow_method_decorator