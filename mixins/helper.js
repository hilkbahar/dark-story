export const helperMixin = {
  methods: {
    getImageUri (id, size, quality) {
      return process.env.mediaBaseUrl + '/images/' + (quality || '75') + '/' + (size || '0x0') + '/' + id
    },
    getIconNameByContentType (type) {
      switch (type) {
        case 'PhotoGallery':
          return 'camera'
        case 'VideoContent':
          return 'play'
        case 'Detail':
          return 'newspaper-o'
      }
    },
    getContentUrl (content) {
      let url = '#' + content.IxName
      if (content.ContentType && content.ContentType === 'Page') {
        url = content.Url
      }
      return url
    },
    openModal (content, url, rootContent) {
      let type = content.ContentType
      if (type === 'Page') return
      if (url) {
        this.$store.dispatch('modal/fetchUrl', url)
      } else {
        this.$store.dispatch('modal/setContent', [content, rootContent])
      }
    },
    share (type, title) {
      console.log(this)
      var url = process.env.siteUrl + this.$route.fullPath
      switch (type) {
        case 'facebook':
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url), 'facebook', 'toolbar=0,status=0,width=650,height=436')
          break
        case 'twitter':
          let twtMessage = title + ' - ' + encodeURI(url)
          window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(twtMessage + ' via @DarkStoryPro'), 'twitter', 'toolbar=0,status=0,width=650,height=436')
          break
      }
    }
  }
}
